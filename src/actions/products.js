import { 
  RETRIEVE_PRODUCTS_SUCCESS,
  RETRIEVE_PRODUCTS_STARTED,
  RETRIEVE_PRODUCTS_FAILURE,
} from "./actiontype/products";

  import axios from 'axios';
  
  export const getProducts = () => {
    return (dispatch, getState) => {
      dispatch(retrieveProductsStarted());
      console.log('current state get products:', getState());

      axios
        .get(`https://apis-dev.aspenku.com/api/v1/product?limit=100&skip=0`, {
            headers: {
              "Content-type": "application/x-wwww-form-urlencoded",
              "Access-Control-Allow-Origin": "*",
              "Authorization": `Basic QXNwZW5rdTpBc3Blbmt1`
            }
          })
        .then(res => {
          dispatch(retrieveProductsSuccess(res.data));
        })
        .catch(err => {
          dispatch(retrieveProductsFailure(err.message));
        });
      
    };
  };
  export const findByTitle = (title) => {
    return (dispatch, getState) => {
      dispatch(retrieveProductsStarted());
      console.log('current state search:', getState());

      axios
        .get(`https://apis-dev.aspenku.com/api/v1/product?search=${title}`, {
            headers: {
              "Content-type": "application/x-wwww-form-urlencoded",
              "Access-Control-Allow-Origin": "*",
              "Authorization": `Basic QXNwZW5rdTpBc3Blbmt1`
            }
          })
        .then(res => {
          dispatch(retrieveProductsSuccess(res.data));
        })
        .catch(err => {
          dispatch(retrieveProductsFailure(err.message));
        });
    }
  }
  
  const retrieveProductsSuccess = products => ({
    type: RETRIEVE_PRODUCTS_SUCCESS,
    payload: {
      ...products
    }
  });
    
  const retrieveProductsStarted = () => ({
    type: RETRIEVE_PRODUCTS_STARTED
  });
  
  const retrieveProductsFailure = error => ({
    type: RETRIEVE_PRODUCTS_FAILURE,
    payload: {
      error
    }
  });