import { 
RETRIEVE_PRODUCT_SUCCESS,
RETRIEVE_PRODUCT_STARTED,
RETRIEVE_PRODUCT_FAILURE,
} from "./actiontype/product";

import axios from 'axios';

export const getProductDetail = (data) => {
    return (dispatch, getState) => {
      dispatch(retrieveProductStarted());
      console.log('current state product detail', getState());

      axios
        .get(`https://apis-dev.aspenku.com/api/v3/product/${data.permalink}`, {
            headers: {
              "Content-type": "application/x-wwww-form-urlencoded",
              "Access-Control-Allow-Origin": "*",
              "Authorization": `Basic QXNwZW5rdTpBc3Blbmt1`
            }
          })
        .then(res => {
          dispatch(retrieveProductSuccess(res.data));
        })
        .catch(err => {
          dispatch(retrieveProductFailure(err.message));
        });
    }
  }

  const retrieveProductSuccess = product => ({
    type: RETRIEVE_PRODUCT_SUCCESS,
    payload: {
      ...product
    }
  });
  
  const retrieveProductStarted = () => ({
    type: RETRIEVE_PRODUCT_STARTED
  });
  
  const retrieveProductFailure = error => ({
    type: RETRIEVE_PRODUCT_FAILURE,
    payload: {
      error
    }
  });