import {
  RETRIEVE_PRODUCT_SUCCESS,
  RETRIEVE_PRODUCT_STARTED,
  RETRIEVE_PRODUCT_FAILURE,
} from "../actions/actiontype/product";

const initialState = {
  loading: false,
  product: [],
  error: null
};

function productReducer(state = initialState, action) {
  switch (action.type) {
    case RETRIEVE_PRODUCT_STARTED:
      return {
        ...state,
        loading: true
      };

    case RETRIEVE_PRODUCT_SUCCESS:
      console.log('success product', action.payload)
      return {
        ...state,
        loading: false,
        error: null,
        product: action.payload.data,
        image: action.payload.data.SpreeProductImages,
      };
    
    case RETRIEVE_PRODUCT_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };

    default:
      console.log('default product')
      return state;
  }
};

export default productReducer;