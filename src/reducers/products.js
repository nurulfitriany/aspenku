import {
  RETRIEVE_PRODUCTS_SUCCESS,
  RETRIEVE_PRODUCTS_STARTED,
  RETRIEVE_PRODUCTS_FAILURE,
} from "../actions/actiontype/products";

const initialState = {
  loading: false,
  products: [],
  error: null
};

function productsReducer(state = initialState, action) {
  switch (action.type) {
    case RETRIEVE_PRODUCTS_STARTED:
      return {
        ...state,
        loading: true
      };
    
    case RETRIEVE_PRODUCTS_SUCCESS:
      console.log('success products', action.payload)
      return {
        ...state,
        loading: false,
        error: null,
        products: action.payload.data.rows
      };
    
    case RETRIEVE_PRODUCTS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };

    default:
      console.log('default product list')
      return state;
  }
};

export default productsReducer;