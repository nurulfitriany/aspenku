import React, { Component } from "react";
import { connect } from "react-redux";
import { getProducts, findByTitle } from "../actions/products";

class ProductsList extends Component {
  constructor(props) {
    super(props);
    this.onChangeSearch = this.onChangeSearch.bind(this);
    this.findByTitle = this.findByTitle.bind(this);

    this.state = {
      searchTitle: "",
    };
  }

  componentDidMount() {
    this.props.getProducts();
  }
  
  onChangeSearch(e) {
    const searchTitle = e.target.value;

    this.setState({
      searchTitle: searchTitle,
    });
  }

  findByTitle() {
    this.props.findByTitle(this.state.searchTitle);
  }
  
  render() {
    const { searchTitle } = this.state;
    const { products: {
        loading, products
    } } = this.props;

    return (
        <div>
            <div>
                <section class="py-5">
                <h4>Product List</h4>
                    <div class="container px-4 px-lg-5 mt-5">
                        <div className="input-group mb-3">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Search by title"
                                value={searchTitle !== '' ? searchTitle : ' '}
                                onChange={this.onChangeSearch}
                            />
                            <div className="input-group-append">
                            <button
                                className="btn btn-outline-secondary"
                                type="button"
                                onClick={this.findByTitle}
                            >
                                Search
                            </button>
                            </div>
                        </div>
                    </div>

                    <div class="container px-4 px-lg-5 mt-5">
                        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
                        { products && products.length > 1 ? products.map(val => {
                                return <div class="col mb-5" key={val.id}>
                                    <div class="card h-100">
                                        <img class="card-img-top" src="https://apis-dev.aspenku.com/product/arabicagayo1.png" alt="..." />
                                        <div class="card-body p-4">
                                            <div class="text-center">
                                                    <h5 class="fw-bolder">
                                                        {val.name}
                                                    </h5>
                                                    $ {val.sell_price}

                                            </div>
                                        </div>
                                        
                                        {console.log('img', val.SpreeProductImages.map(val => Object.values(val)[1]))}
                                        <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                            <div class="text-center"><a class="btn btn-outline-dark mt-auto" href={`/productdetail/${val.permalink}`}>View</a></div>
                                        </div>
                                    </div>
                                </div>
                        }) : <h5 class="fw-bolder">No Available Data</h5>
                        }

                        
                        </div>
                    </div>
                </section>
            </div>
        </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      products: state.products,
    };
  };

export default connect(mapStateToProps, { getProducts, findByTitle })(ProductsList);