import React, { Component } from "react";
import { connect } from "react-redux";
import { getProductDetail } from "../actions/product";

class ProductView extends Component {
  
  componentDidMount() {
    this.props.getProductDetail(this.props.match.params);
  }
  
  render() {
    const { 
        image,
        product: {
            loading, product
        } ,   
    } = this.props;

    return (
            <div class="container">
            <div class="card-product">
                <div class="container-fliud">
                    <div class="wrapper row">
                        <div class="preview col-md-6">
                            <div class="preview-pic tab-content">
                            <div class="tab-pane active" id="pic-1"><img src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" /></div>
                            <div class="tab-pane" id="pic-2"><img src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" /></div>
                            <div class="tab-pane" id="pic-3"><img src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" /></div>
                            <div class="tab-pane" id="pic-4"><img src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" /></div>
                            <div class="tab-pane" id="pic-5"><img src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" /></div>
                            </div>
                            <ul class="preview-thumbnail nav nav-tabs">
                            <li class="active"><a data-target="#pic-1" data-toggle="tab"><img src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" /></a></li>
                            <li><a data-target="#pic-2" data-toggle="tab"><img src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" /></a></li>
                            <li><a data-target="#pic-3" data-toggle="tab"><img src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" /></a></li>
                            </ul>
                        </div>

                        <div class="details col-md-6">
                            <h3 class="product-title">{product.name}</h3>
                            <p class="product-description">{product.short_description}</p>
                            <h4 class="price"><span>${product.sell_price}</span></h4>
                            <div class="action">
                                <button class="add-to-cart btn btn-default" type="button">add to cart</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
  }
}

const mapStateToProps = (state) => {
    return {
      product: state.product,
      image: state.image
    };
  };

export default connect(mapStateToProps, { getProductDetail })(ProductView);